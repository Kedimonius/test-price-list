/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});


function ss(id)
{
    // let article = document.getElementById('aUpdate');

    // document.getElementById('update_name').value=article.dataset.name;
    // document.getElementById('update_unit').value=article.dataset.unit;
    // document.getElementById('update_price').value=article.dataset.price;
    // document.getElementById('update_quantity').value=article.dataset.quantity;
    alert('ghtr');
    document.getElementById('update_id').value=id;
    // alert(article.dataset.href)
}

function saveOnClick(route){
    $value=$(this).val();
    console.log('$value');
    $.ajax({
        type: 'get',
        url: route,
        data: {'search': $value},
        headers: {'csrftoken': '{{ csrf_token() }}'},
        success: function (data) {
            $('tbody').html(data);
        }
    })
}

// <script type="text/javascript">
//     $('#search').on('keyup',function(){
//         $value=$(this).val();
//         console.log('$value');
//         $.ajax({
//             type : 'get',
//             url : '{{route('search')}}',
//             data:{'search':$value},
//             success:function(data){
//                 $('tbody').html(data);
//             }
//         });
//     })


