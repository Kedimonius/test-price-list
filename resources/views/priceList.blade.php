@extends('layouts.app')

@section('content')
    <div class="container">
        <div class='row'>
            <h1>Прайс лист</h1>
        </div>
        <div class='row'>
            <button type="button" class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#addProduct">
                 Добавить продукт
            </button>
        </div>
        <br />
        <div class='row'>
            <input type="text" class="form-control" id="search" name="search" onkeyup="searchOnClick( '{{ route ('search') }}' )" placeholder="Поиск">
        </div>
        <br />
        <div class='row @if(count($priceList)!= 0) show @else display-hidden @endif' id='products-wrap'>
            <table class="table table-striped ">
                <thead>
                <tr>
                    <th>Наименование продукции</th>
                    <th>Ед. изм.</th>
                    <th>Цена</th>
                    <th>Кол-во на складе</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($priceList as $priceL)
                    <tr>
                        <td>{{ $priceL->name }}</td>
                        <td>{{ $priceL->unit }}</td>
                        <td>{{ $priceL->price }}</td>
                        <td>{{ $priceL->quantity }}</td>
                        <td><a href="{{ route('deleteProduct', ['id' => $priceL->id]) }}">Удалить</a></td>
                        <td><a data-toggle="modal" onclick="setIdForUpdate( {{ $priceL->id }} )" data-target="#updateProduct"
                               data-id="{{ $priceL->id }}" href="" id="aUpdate" >Изменить</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="alert alert-warning @if(count($priceList) != 0) display-hidden @else show @endif" role="alert"> Записей нет</div>
        </div>
    </div>

    <!-- Modal Add Product-->
    <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="addProductLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addProductLabel">Добавление продукта</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action=" {{ route('addProduct') }} " method="GET">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Название продукта</label>
                        <textarea class="form-control" id="name" name="name"></textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="unit">Единица измерения</label>
                        <input type="text" class="form-control" id="unit" name="unit">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="price">Цена</label>
                        <input type="number" step="0.01" class="form-control" id="price" name="price">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="quantity">Количество на складе</label>
                        <input type="number" step="0.01" class="form-control" id="quantity" name="quantity">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" >Сохранить</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Update Product -->
    <div class="modal fade" id="updateProduct" tabindex="-1" role="dialog" aria-labelledby="updateProductLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="updateProductLabel">Изменение продукта</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action=" {{ route('updateProduct') }} " method="GET" id="updateForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="update_name">Название продукта</label>
                            <textarea class="form-control" id="update_name" name="name"></textarea>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="update_unit">Единица измерения</label>
                            <input type="text" class="form-control" id="update_unit" name="unit">
                            <input type="text" class="display-hidden" id="update_id" name="id" value="">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="update_price">Цена</label>
                            <input type="number" step="0.01" class="form-control" id="update_price" name="price">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="update_quantity">Количество на складе</label>
                            <input type="number" step="0.01" class="form-control" id="update_quantity" name="quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary" >Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
    function setIdForUpdate (id)
    {
    // let article = document.getElementById('aUpdate');

    // document.getElementById('update_name').value=article.dataset.name;
    // document.getElementById('update_unit').value=article.dataset.unit;
    // document.getElementById('update_price').value=article.dataset.price;
    // document.getElementById('update_quantity').value=article.dataset.quantity;
    document.getElementById('update_id').value=id;
    // alert(article.dataset.href)
    }
    </script>

    <script>
        function searchOnClick (url)
        {
            var $value = $('#search').val();
            $.ajax({
                type: 'get',
                url: url,
                data: {'search': $value},
                success: function (data) {
                    $('tbody').html(data);
                }
            })
        }
    </script>

{{--        <script type="text/javascript">--}}
{{--            $('#search').on('keyup',function(){--}}
{{--                $value=$(this).val();--}}
{{--                console.log('$value');--}}
{{--                $.ajax({--}}
{{--                    type : 'get',--}}
{{--                    url : '{{route('search')}}',--}}
{{--                    data:{'search':$value},--}}
{{--                    success:function(data){--}}
{{--                        $('tbody').html(data);--}}
{{--                    }--}}
{{--                });--}}
{{--            })--}}
{{--        </script>--}}
    {{--    <script type="text/javascript">--}}
    {{--        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });--}}
    {{--    </script>--}}

@endsection


