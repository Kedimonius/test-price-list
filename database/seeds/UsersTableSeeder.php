<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'qwer',
            'email' => 'qwer@mail.ru',
            'email_verified_at' => now(),
            'password' => '$2y$10$UdNV8ofJm9vloenNr1Sci.B.oG4hspruWG6lF5vRMLd0lhm0zF3UW', // password
            'remember_token' => Str::random(10),
        ]);
        DB::table('users')->insert([
            'name' => 'asdf',
            'email' => 'asdf@mail.ru',
            'email_verified_at' => now(),
            'password' => '$2y$10$UdNV8ofJm9vloenNr1Sci.B.oG4hspruWG6lF5vRMLd0lhm0zF3UW', // password
            'remember_token' => Str::random(10),
        ]);
    }
}
