<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Показывает страницус прайс листом.
     *
     * @return Renderable
     */
    public function show()
    {
        $priceList = DB::table('products')
            ->leftJoin('units', 'products.unit_id', '=', 'units.id')
            ->where('products.user_id', '=', Auth::id())
            ->select('products.name as name',
                             'products.price as price',
                             'products.quantity as quantity',
                             'units.unit as unit',
                             'products.id as id')
            ->get();

        return view('priceList', ['priceList' => $priceList]);
    }

    /**
     * Удаляет продукт из таблицы по индентификатору продукта.
     *
     * @return RedirectResponse
     */
    public function deleteProduct(Request $request)
    {
        $input = $request->all();
        if (! ($input && $input['id'])) {
            abort(403);
        }
        Product::find($input['id'])->delete();

        return redirect('price_list');
    }

    /**
     * Обновляет информацию о продукте.
     * Если такой продукт уже существует, то он затирает старую запись.
     *
     * @return RedirectResponse
     */
    public function updateProduct(Request $request)
    {
        $input = $request->all();
        if (! ($input && $input['id'])) {
            abort(403);
        }
        $oldProduct = Product::find($input['id']);
        $unitId =isset($input['unit']) ? Unit::getUnit($input['unit'])->id : $oldProduct->unit_id;

        if (isset($input['unit']) || isset($input['name'])) {
            $newProduct = Product::getProduct(isset($input['name']) ? $input['name'] :  $oldProduct->name, Auth::id(), $unitId);
        }
        if (isset($newProduct)) {
            $oldProduct->delete();
            $newProduct->price = isset($input['price']) ? $input['price'] :  $oldProduct->price;
            $newProduct->quantity = isset($input['quantity']) ? $input['quantity'] :  $oldProduct->quantity;
            $newProduct->save();
        } else {
            $oldProduct->price = isset($input['price']) ? $input['price'] :  $oldProduct->price;
            $oldProduct->quantity = isset($input['quantity']) ? $input['quantity'] :  $oldProduct->quantity;
            $oldProduct->name = isset($input['name']) ? $input['name'] :  $oldProduct->name;
            $oldProduct->unit_id = $unitId;
            $oldProduct->save();
        }

        return redirect('price_list');
    }

    /**
     * Создает запись о продукте.
     * Если она уже существовала, то обновляет цену и количество продукта.
     *
     * @return RedirectResponse
     */
    public function addProduct(Request $request)
    {
        $userId = Auth::id();
        if (! $userId) {
            abort(403);
        }

        $input = $request->all();
        $unit = Unit::getUnit(isset($input['unit'])?$input['unit']:abort(403));

        $product = Product::getProduct($input['name'], $userId, $unit->id);
        if (! $product) {
            Product::create([
                'name' => isset($input['name'])?$input['name']:abort(403),
                'price' => isset($input['price'])?$input['price']:abort(403),
                'quantity' => isset($input['quantity'])?$input['quantity']:abort(403),
                'user_id' => $userId,
                'unit_id' => $unit->id,
            ]);
        }

        return redirect('price_list');
    }

    public function search(Request $request)
    {

        if ($request->ajax()) {
            $output = "";
            $user = Auth::id();
            $priceList = DB::table('products')
                ->leftJoin('units', 'products.unit_id', '=', 'units.id')
                ->where('products.user_id', '=', $user)
                ->select('products.name as name',
                    'products.price as price',
                    'products.quantity as quantity',
                    'units.unit as unit',
                    'products.id as id')
                ->where('name', 'LIKE', '%' . $request->search . "%")
                ->get();
            if ($priceList) {
                foreach ($priceList as $key => $product) {
                    $output .= '<tr>' .
                        '<td>' . $product->name . '</td>' .
                        '<td>' . $product->unit . '</td>' .
                        '<td>' . $product->price . '</td>' .
                        '<td>' . $product->quantity . '</td>' .
                        '<td><a href=' . route('deleteProduct', ['id' => $product->id]) .'>Удалить</a></td>'.
                        '<td><a data-toggle="modal" onclick="  setIdForUpdate(' . $product->id . ') " data-target="#updateProduct" data-id="' . $product->id . '" href="">Изменить</a></td>'.

                    '</tr>';
                }

            }
            return Response($output);
        }
    }
}
