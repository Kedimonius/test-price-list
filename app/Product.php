<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    /**
     * Таблица модели.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Поля модели, которые можно заполнять.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'unit_id', 'price', 'quantity', 'user_id'
    ];

    /**
     * Создает связь один ко многим с моделью Unit.
     *
     * @return BelongsTo
     */
    public function units()
    {
        return $this->belongsTo('App\Unit');
    }

    /**
     * Создает связь один ко многим с моделью User.
     *
     * @return BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Получает модель продукта по уникальным полям таблицы.
     *
     * @var string $name   наименование продукта
     * @var int    $userId идентификатор пользователя
     * @var int    $unitId идентификатор единицы измерения
     *
     * @return Product|null
     */
    public static function getProduct($name, $userId, $unitId)
    {
        return Product::where([
            ['name'   , $name],
            ['user_id', $userId],
            ['unit_id', $unitId],
        ])->first();
    }
}
