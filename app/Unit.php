<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Unit extends Model
{
    /**
     * Таблица модели.
     *
     * @var string
     */
    protected $table = 'units';

    /**
     * Поля модели, доступные к заполнению.
     *
     * @var array
     */
    protected $fillable = [
        'unit'
    ];

    /**
     * Создает связь один ко многим с моделью Product.
     *
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }

    /**
     * Получает модель единицы измерения по уникальным полям таблицы.
     * В случае, если единица измерения не найдена, создает запись в таблице.
     *
     * @var string $inputUnit   наименование единицы измерения
     *
     * @return Unit
     */
    public static function getUnit($inputUnit)
    {
        $unit = Unit::where('unit', $inputUnit)->first();
        if (! $unit) {
            $unit = Unit::create(['unit' => $inputUnit]);
        }
        return $unit;
    }
}
